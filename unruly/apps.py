from django.apps import AppConfig
from django.db import models


class UnrulyConfig(AppConfig):
    name = 'unruly'

    def ready(self):
        from pizzas.models import Pizza
        creator_name = models.CharField(max_length=100, blank=True, null=True)
        creator_name.contribute_to_class(Pizza, 'creator_name')

