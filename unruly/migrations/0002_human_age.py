# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    migrated_app = 'animals'

    dependencies = [
        ('animals', '0001_initial'),
        ('unruly', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='human',
            name='age',
            field=models.PositiveSmallIntegerField(blank=True, null=True),
        ),
    ]
