# Migrate other app Sample

This is just a sample project to show the use of a Django feature request. Please do not clone unless directly concerned by the matter.

To be able to run the application copy ``environ.py.dist`` to ``environ.py`` and adjust your settings.