from django.contrib import admin

from .models import Topping, Pizza


class PizzaToppingInline(admin.TabularInline):
  model = Pizza.toppings.through


@admin.register(Topping)
class ToppingAdmin(admin.ModelAdmin):
    pass


@admin.register(Pizza)
class PizzaAdmin(admin.ModelAdmin):

    inlines = [
      PizzaToppingInline,
    ]
    exclude = ('toppings', )
