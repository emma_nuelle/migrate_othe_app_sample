from django.db import models


class Topping(models.Model):

    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Topping"
        verbose_name_plural = "Toppings"

    def __str__(self):
        return self.name


class Pizza(models.Model):

    name = models.CharField(max_length=100)
    description = models.TextField()
    toppings = models.ManyToManyField(Topping, related_name='used_on', blank=True)
    price = models.DecimalField(max_digits=4, decimal_places=2, default=10)

    class Meta:
        verbose_name = "Pizza"
        verbose_name_plural = "Pizzas"

    def __str__(self):
        return self.name
